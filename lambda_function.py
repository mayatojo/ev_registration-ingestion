import urllib.request
import shutil
from datetime import datetime

from util.general import date2name
from util.aws_s3 import upload_file, connect_s3

s3_resource = connect_s3()

# Description:
# ______________________________________________________________

# Once a quarter, this script downloads the public EV registration data
# and saves the file as it is in AWS S3.

# EV_Registration script order:
# 1. EV_Registration-Ingestion (this script)
# 2. EV_Registration-Transformation 
# 3. EV_Registration-Upload

# Period of refresh: quarterly

# ______________________________________________________________




def lambda_handler(event, context):
    # get full htmls and from there filter out our the VEH0134-URL
    url = 'https://www.gov.uk/government/statistical-data-sets/all-vehicles-veh01'
    file_type = 'VEH0134'
    html = get_html(url)
    file_url = html.split(file_type)[1].split('href="')[1].split('"')[0]

    # upload to S3
    date = datetime.now().date()
    date_folder = date2name(date, format='YYYY_Q0X')
    folder = '1_RawData/EV_registrations/' + date_folder + '/'
    bucket = 'ev-modeling-platform'
    key = folder + file_type.lower() + '.ods'

    download_to_s3(file_url, s3_resource, bucket, s3_location=key)


def get_html(url):
    response = urllib.request.urlopen(url)
    data = response.read()  # a `bytes` object
    html = data.decode(
        'utf-8')  # a `str`; this step can't be used if data is binary
    return html


def download_to_s3(url, s3_resource, s3_bucket, s3_location):
    response = urllib.request.urlopen(url)
    uploadByteStream = response.read()  # bytes object
    upload_file(s3_resource, s3_location, body=uploadByteStream)