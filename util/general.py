def date2quarter(date):
    return (date.month-1)//3 +1


def date2name(date, format='YYYY_WXX'):
    """date: datetime.date e.g. datetime.now().date()"""
    if format=='YYYY_WXX':
        year = str(date.isocalendar()[0])
        week = str(date.isocalendar()[1]).zfill(2)
        name = year + '_W' + week
    elif format=='YYYY_MXX':
        year = str(date.isocalendar()[0])
        month = str(date.month).zfill(2)
        name = year + '_M' + month
    elif format=='YYYY_QX':
        year = str(date.isocalendar()[0])
        quarter = str(date2quarter(date))
        name = year + '_Q' + quarter
    else:
        print('unknown format, return date as string')
        return str(date)
    return name